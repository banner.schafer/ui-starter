
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Hello Template</title>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="description" content=""><!-- <link rel="icon" href="favicon.png"> -->

  <script src="/static/htmx.min.js"></script><!-- CSS Reset -->
  <script src="/static/hyper.min.js"></script><!-- CSS Reset -->
  <link rel="stylesheet" href="/static/pico.min.css"><!-- Milligram CSS -->
</head>
<body hx-boost="true">
  <main class="container">
    {{embed}}
  </main>
</body>
</html>
