package blog

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/etag"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/template/html/v2"
)

func Main() int {

	engine := html.New("./views", ".tpl")
	engine.Debug(true)

	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
		Immutable:   true,
		BodyLimit:   100 * 1024, // limit body size to 100 kb
	})

	app.Use(recover.New())
	app.Use(compress.New())
	app.Use(etag.New())

	app.Static("/static", "./static")

	app.Get("/", handleIndex())

	err := app.Listen(":8080")
	if err != nil {
		log.Println(err)
		return 1
	}

	return 0
}

func handleIndex() fiber.Handler {
	return func(c *fiber.Ctx) error {
		return c.Render("index", fiber.Map{})
	}
}
